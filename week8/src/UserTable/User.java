/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserTable;

/**
 *
 * @author informatics
 */
public class User {
    int id;
    String login;
    String name;
    String surname;
    String password;
    String tel;
    int age;
    float weight;
    float height;
    
    public User(){
        
    }

    public User(int id, String login, String name, String surname, String password, String tel, int age, float weight, float height) {
        this.id = id;
        this.login = login;
        this.name = name;
        this.surname = surname;
        this.password = password;
        this.tel = tel;
        this.age = age;
        this.weight = weight;
        this.height = height;
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPassword() {
        return password;
    }

    public String getTel() {
        return tel;
    }

    public int getAge() {
        return age;
    }

    public float getWeight() {
        return weight;
    }

    public float getHeight() {
        return height;
    }
    
    
}
